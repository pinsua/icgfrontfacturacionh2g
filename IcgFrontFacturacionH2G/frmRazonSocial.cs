﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgFrontFacturacionH2G
{
    public partial class frmRazonSocial : Form
    {
        public string _rSocial;

        public frmRazonSocial()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtrSocial.Text))
            {
                _rSocial = txtrSocial.Text;
                this.Close();
            }
            else
            {
                MessageBox.Show("La Razón Social no puede ser blanco o nula. Por favor ingresela nuevamente.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                txtrSocial.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Si cancela esta operación el ticket no se imprimirá y deberá imprimirlo luego. Desea cancelar la operción?",
                "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                _rSocial = "";
                this.Close();
            }
            else
            {
                txtrSocial.Text = "";
            }
        }
    }
}
