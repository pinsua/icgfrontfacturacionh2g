﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgFrontFacturacionH2G
{
    public partial class frmCuit : Form
    {
        public string _cuit;

        public frmCuit()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int _digitoValidador = Hasar2daGen.FuncionesVarias.CalcularDigitoCuit(txtCuit.Text.Replace("-", ""));
            int _digitorecibido = Convert.ToInt16(txtCuit.Text.Substring(txtCuit.Text.Length - 1));
            if (_digitorecibido == _digitoValidador)
            {
                _cuit = txtCuit.Text;
                this.Close();
            }
            else
            {
                MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo nuevamente.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                txtCuit.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("El CUIT ingresado no es correcto. Si cancela esta operación el ticket no se imprimirá y deberá imprimirlo luego. Desea cancelar la operción?",
                "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                _cuit = "";
                this.Close();
            }
            else
            {
                txtCuit.Text = "";
            }
        }
    }
}
