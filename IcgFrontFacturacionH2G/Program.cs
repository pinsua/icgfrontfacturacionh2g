﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Configuration;
using Hasar2daGen;

namespace IcgFrontFacturacionH2G
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _serie = "";
            string _numero = "";
            string _n = "";
            int _tipo = 12;
            int _accion = 0;
            string _textoRegalo1 = "";
            string _textoRegalo2 = "";
            string _textoRegalo3 = "";
            string _caja = "";
            string _terminal = "";

        string _ip = System.Configuration.ConfigurationManager.AppSettings["Ip"];

            if (File.Exists("fiscal10.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("fiscal10.xml");

                XmlNodeList _List1 = xDoc.SelectNodes("/doc/bd");
                foreach (XmlNode xn in _List1)
                {
                    _server = xn["server"].InnerText;
                    _database = xn["database"].InnerText;
                    _user = xn["user"].InnerText;
                }

                XmlNodeList _List2 = xDoc.SelectNodes("/doc");
                foreach (XmlNode xn in _List2)
                {
                    _codVendedor = xn["codvendedor"].InnerText;
                    _tipodoc = xn["tipodoc"].InnerText;
                    _serie = xn["serie"].InnerText;
                    _numero = xn["numero"].InnerText;
                    _n = xn["n"].InnerText;
                }

                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xConfig = new XmlDocument();
                    xConfig.Load("Config2daGeneracion.xml");

                    XmlNodeList _ListCfg = xConfig.SelectNodes("/config");
                    foreach (XmlNode xn in _ListCfg)
                    {
                        _ip = xn["ip"].InnerText;
                        _caja = xn["caja"].InnerText;
                        _textoRegalo1 = xn["tktregalo1"].InnerText;
                        _textoRegalo2 = xn["tktregalo2"].InnerText;
                        _textoRegalo3 = xn["tktregalo3"].InnerText;
                        _terminal = xn["terminal"].InnerText;
                    }                    

                    //Solo imprimimos las que N = B
                    if (_n.ToUpper() == "B")
                    {
                        //Armamos el stringConnection.
                        string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                        //Conectamos.
                        using (SqlConnection _connection = new SqlConnection(strConnection))
                        {
                            try
                            {
                                _connection.Open();

                                //Busco el comprobante en FacturasVentaSerieResol.
                                Hasar2daGen.FacturasVentaSerieResol _fsr = Hasar2daGen.FacturasVentaSerieResol.GetTiquet(_serie, Convert.ToInt32(_numero), _n, _connection);
                                //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                if (_fsr.numeroFiscal == 0)
                                {
                                    //Recuperamos los datos de la cabecera.
                                    Hasar2daGen.Cabecera _cab = Hasar2daGen.Cabecera.GetCabecera(_serie, Convert.ToInt32(_numero), _n, _connection);
                                    List<Hasar2daGen.Items> _items = Hasar2daGen.Items.GetItems(_cab.numserie, _cab.n, _cab.numalbaran, _connection);
                                    List<Hasar2daGen.OtrosTributos> _oTributos = Hasar2daGen.OtrosTributos.GetOtrosTributos(_serie, _n, Convert.ToInt32(_numero), _connection);
                                    List<Hasar2daGen.Recargos> _recargos = Hasar2daGen.Recargos.GetRecargos(_serie, _n, Convert.ToInt32(_numero), _connection);

                                    if (_items.Count > 0)
                                    {
                                        List<Hasar2daGen.Pagos> _pagos = Hasar2daGen.Pagos.GetPagos(_cab.numserie, _cab.numfac, _cab.n, _connection);

                                        if (_pagos.Count > 0)
                                        {
                                            bool _cuitOK = true;
                                            bool _rSocialOK = true;
                                            bool _hasChange = false;
                                            //Vemos si debemos validar el cuit.
                                            if (_cab.regfacturacioncliente != 4)
                                            {

                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cab.nrodoccliente.Replace("-", ""));
                                                int _digitorecibido = Convert.ToInt16(_cab.nrodoccliente.Substring(_cab.nrodoccliente.Length - 1));
                                                if (_digitorecibido == _digitoValidador)
                                                    _cuitOK = true;
                                                else
                                                {
                                                    frmCuit _frm = new frmCuit();
                                                    _frm.ShowDialog();
                                                    if (String.IsNullOrEmpty(_frm._cuit))
                                                        _cuitOK = false;
                                                    else
                                                    {
                                                        _cab.nrodoccliente = _frm._cuit;
                                                        _cuitOK = true;
                                                    }
                                                    _frm.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cab.nrodoccliente))
                                                    _cab.nrodoccliente = "11111111";
                                            }

                                            //Validamos la direccion.
                                            if (String.IsNullOrEmpty(_cab.direccioncliente))
                                                _cab.direccioncliente = ".";

                                            //Vaidamos la razon social.
                                            if (String.IsNullOrEmpty(_cab.nombrecliente))
                                            {
                                                frmRazonSocial _frm = new frmRazonSocial();
                                                _frm.ShowDialog();
                                                if (String.IsNullOrEmpty(_frm._rSocial))
                                                    _rSocialOK = false;
                                                else
                                                {
                                                    _cab.nombrecliente = _frm._rSocial;
                                                    _rSocialOK = true;
                                                }
                                                _frm.Dispose();
                                            }

                                            //Vaidamos la razon social.
                                            if (String.IsNullOrEmpty(_cab.nombrecliente))
                                            {
                                                frmRazonSocial _frm = new frmRazonSocial();
                                                _frm.ShowDialog();
                                                if (String.IsNullOrEmpty(_frm._rSocial))
                                                    _rSocialOK = false;
                                                else
                                                {
                                                    _cab.nombrecliente = _frm._rSocial;
                                                    _rSocialOK = true;
                                                }
                                                _frm.Dispose();
                                            }

                                            switch (_cab.descripcionticket.Substring(0, 3))
                                            {
                                                case "003":
                                                case "008":
                                                    {
                                                        if (_cuitOK)
                                                        {
                                                            if (_rSocialOK)
                                                            {
                                                                //Busco documentos Asociados.
                                                                List<Hasar2daGen.DocumentoAsociado> _docAsoc = Hasar2daGen.DocumentoAsociado.GetDocumentoAsociado(_items[0]._abonoDeNumseie, _items[0]._abonoDe_N, _items[0]._abonoDeNumAlbaran, _connection);
                                                                bool _faltaPapel;
                                                                string _respImpre = Hasar2daGen.Impresiones.ImprimirNotaCreditoAB(_cab, _items, _pagos, _oTributos, _recargos, _docAsoc, _ip, _connection, out _faltaPapel);
                                                                if (!String.IsNullOrEmpty(_respImpre))
                                                                    MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                if (_faltaPapel)
                                                                    MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }
                                                            else
                                                                MessageBox.Show("El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                        }
                                                        else
                                                            MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                        break;
                                                    }
                                                case "001":
                                                case "006":
                                                    {
                                                        if (_cuitOK)
                                                        {
                                                            if (_rSocialOK)
                                                            {
                                                                bool _faltaPapel;
                                                                string _respImpre = Hasar2daGen.Impresiones.ImprimirFacturasAB(_cab, _items, _pagos, _oTributos, _recargos, _ip, _connection, out _faltaPapel);
                                                                if (!String.IsNullOrEmpty(_respImpre))
                                                                    MessageBox.Show(_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                if (_faltaPapel)
                                                                    MessageBox.Show("La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }
                                                            else
                                                                MessageBox.Show("El Cliente no posee Razon Social. Por favor ingresela y luego reimprimar el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                        else
                                                            MessageBox.Show("El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                                case "002":
                                                case "007":
                                                    {
                                                        //Notas de Debito.
                                                        MessageBox.Show("Notas de Debito. Aún no estan configuradas.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        MessageBox.Show("Tipo de Documento no configurado para su fiscalización.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                            }

                                            //vemos si tenemos modificacion.
                                            if (_hasChange)
                                            {
                                                Hasar2daGen.Clientes.UpdateCliente(_cab.nombrecliente, _cab.nrodoccliente, _cab.codcliente, _connection);
                                                Hasar2daGen.RemTransacciones.GraboRemTransacciones(_terminal, _caja, _cab.z.ToString(), _tipo, _accion, "", _cab.codcliente.ToString(), _cab.n, _connection);
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("No existen Items registrados. Por favor revise el comprobante.",
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    }
                                }
                                else
                                {

                                }
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show("Se produjo el siguiente error: " + ex.Message,
                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el archivo de configuración. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }

        }

        class Impresiones
        {


            //public static bool FechaOk (hfl.argentina.HasarImpresoraFiscalRG3561 _cf, Cabecera _cab)
            //{
            //    bool _rta = false;
            //    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora _respFechaHora = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora();
            //    _respFechaHora = _cf.ConsultarFechaHora();

            //    DateTime _fecha = _respFechaHora.getFecha();

            //    if (_cab.fecha.Date == _fecha.Date)
            //        _rta = true;

            //    return _rta;
            //}
        }
    }
}
